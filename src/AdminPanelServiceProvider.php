<?php

namespace Cuatrokb\AdminPanel;

use Cuatrokb\AdminPanel\Console\Commands\AdminPanelInitializeEnv;
use Cuatrokb\AdminPanel\Console\Commands\AdminPanelInstall;
use Illuminate\Support\ServiceProvider;

class AdminPanelServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->commands([
            AdminPanelInitializeEnv::class,
            AdminPanelInstall::class,
        ]);

        if ($this->app->runningInConsole()) {
            if (!class_exists('FillDefaultAdminUserAndPermissions')) {
                $timestamp = date('Y_m_d_His', time() + 5);

                $this->publishes([
                    __DIR__ . '/../install-stubs/database/migrations/fill_default_admin_user_and_permissions.php' => database_path('migrations') . '/' . $timestamp . '_fill_default_admin_user_and_permissions.php',
                ], 'migrations');
            }
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
